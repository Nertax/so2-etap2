//
// Created by root on 12.04.19.
//

#include "DiningPhilosophersProblem.hpp"



std::default_random_engine DiningPhilosophersProblem::generator;
bool DiningPhilosophersProblem::endKeyPressed = false;
std::vector<Fork> DiningPhilosophersProblem::forks;
std::vector<ThreadCycleStatus> DiningPhilosophersProblem::statusOfThreads;
std::mutex DiningPhilosophersProblem::forksMutex;


void DiningPhilosophersProblem::philosopherCycle(int philosopherNumber)
{

    std::uniform_int_distribution<int> distribution(3500, 6500);
    bool leftForkTaken = false;
    bool rightForkTaken = false;

    while(!endKeyPressed)
    {

        long timeForAction = distribution(generator);

        //mysli
        statusOfThreads[philosopherNumber].Action = PhilosopherAction::Think;
        statusOfThreads[philosopherNumber].ActionProgress = 0;

        uint a = 10;
        for(int i = 0; i < a; i++)
        {
            statusOfThreads[philosopherNumber].ActionProgress = ( i * 100 / a );
            std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction  / a));

        }
        statusOfThreads[philosopherNumber].ActionProgress = 100;



        //bierze lewy widelec
        //ustaw status watku na bierze lewy widelec
        statusOfThreads[philosopherNumber].Action = PhilosopherAction::TakeLeftFork;
        statusOfThreads[philosopherNumber].ActionProgress = 0;
        while(leftForkTaken == false)
        {
            forksMutex.lock();
            if(!forks[philosopherNumber].isTaken)
            {
                leftForkTaken = true;
                forks[philosopherNumber].isTaken = true;
                forks[philosopherNumber].NumberOfThreadThatHoldThisFork = philosopherNumber;
            }
            forksMutex.unlock();
        }


        long timeForTakingRightFork = distribution(generator);
        struct timespec aa, b;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &aa);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &b);

        //bierze prawy widelec
        //ustaw status watku na bierze prawy widelec
        statusOfThreads[philosopherNumber].Action = PhilosopherAction::TakeRightFork;
        statusOfThreads[philosopherNumber].ActionProgress = 0;
        while(rightForkTaken == false && Time::TimeDiff(aa, b) < (timeForTakingRightFork * 1000000))
        {

            forksMutex.lock();
            if(philosopherNumber == forks.size() - 1) //ostatni watek
            {
                if(!forks[0].isTaken)
                {
                    rightForkTaken = true;
                    forks[0].isTaken = true;
                    forks[0].NumberOfThreadThatHoldThisFork = philosopherNumber;

                }
            }
            else // od 0 do przedostatniego watku
            {
                if(!forks[philosopherNumber + 1].isTaken)
                {
                    rightForkTaken = true;
                    forks[philosopherNumber + 1].isTaken = true;
                    forks[philosopherNumber + 1].NumberOfThreadThatHoldThisFork = philosopherNumber;
                }
            }

            forksMutex.unlock();
            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &b);
            statusOfThreads[philosopherNumber].ActionProgress = (double)Time::TimeDiff(aa, b) / (timeForTakingRightFork * 1000000) * 100;
        }


        //antydeadlock i antylivelock ficzer
        if(rightForkTaken == false)
        {
            //odkladam lewy
            forksMutex.lock();
            forks[philosopherNumber].isTaken = false;
            forks[philosopherNumber].NumberOfThreadThatHoldThisFork = -1;
            leftForkTaken = false;
            forksMutex.unlock();

            continue;
        }

        //je
        statusOfThreads[philosopherNumber].Action = PhilosopherAction::Eat;
        statusOfThreads[philosopherNumber].ActionProgress = 0;
        timeForAction = distribution(generator);

        a = 10;
        for(int i = 0; i < a; i++)
        {
            statusOfThreads[philosopherNumber].ActionProgress = ( i * 100 / a );
            std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction / a));

        }
        statusOfThreads[philosopherNumber].ActionProgress = 100;



        //odklada widelce
        forksMutex.lock();

        //oklada lewy widelec
        forks[philosopherNumber].isTaken = false;
        forks[philosopherNumber].NumberOfThreadThatHoldThisFork = -1;
        leftForkTaken = false;

        //odklada prawy wiedelec
        if(philosopherNumber == forks.size() - 1) //ostatni watek
        {
            forks[0].isTaken = false;
            forks[0].NumberOfThreadThatHoldThisFork = -1;
            rightForkTaken = false;

        }
        else // od 0 do przedostatniego watku
        {
            forks[philosopherNumber + 1].isTaken = false;
            forks[philosopherNumber + 1].NumberOfThreadThatHoldThisFork = -1;
            rightForkTaken = false;
        }

        forksMutex.unlock();

    }
}
