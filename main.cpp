//Daniel Pawelko 235666 WT 15:15 TN

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <random>
#include <ncurses.h>
#include "Time.hpp"
#include "DiningPhilosophersProblem.hpp"





int main(int argc, char **argv) {

    int threadsAmount;


    if (argc != 2) {

        std::cout << "Podaj liczbe watkow" << std::endl;
        return 3;
    }
    else {

        std::string str(argv[1]);
        threadsAmount = std::stoi(str);
    }


    if(threadsAmount <= 2) {

        std::cout << "Podales liczbe watkow mniejsza od 2!" << std::endl;
        return 1;
    }

    if(threadsAmount >= 50) {

        std::cout << "Zbyt duza liczba watkow, podaj liczbe mniejsza niz 50" << std::endl;
        return 2;
    }


    //inicjalizacja ncurses
    initscr();
    cbreak();
    noecho();
//    raw();
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);


    //tworzymy widelce i status watkow
    DiningPhilosophersProblem::forks.resize(threadsAmount);
    DiningPhilosophersProblem::statusOfThreads.resize(threadsAmount);


    //tworzymy watki - filozofow
    std::vector<std::thread> threads(threadsAmount);
    for(int i = 0; i < threadsAmount; i++) {
        threads[i] = std::thread(DiningPhilosophersProblem::philosopherCycle, i);
    }



    int kolumny = 0;
    int rzedy = 0;
    getmaxyx( stdscr, rzedy, kolumny );


    //glowna petla programu
    char c = 0;
    while(c != 'q')
    {
        clear();

        c = getch();

        mvprintw(2, 5, "Wolne widelce: ");

        for (int j = 0; j < threadsAmount; ++j) {

            DiningPhilosophersProblem::forksMutex.lock();


            if(!DiningPhilosophersProblem::forks[j].isTaken)
                printw("F%d ", j);

            DiningPhilosophersProblem::forksMutex.unlock();
        }

        for(int i = 0; i < threadsAmount; i++) {

            switch (DiningPhilosophersProblem::statusOfThreads[i].Action) {

                case PhilosopherAction::Think :
                    mvprintw(4 + i, 5, "Watek %d: Mysli %d\%, widelce: ", i,
                             DiningPhilosophersProblem::statusOfThreads[i].ActionProgress); //2
                    break;

                case PhilosopherAction::TakeLeftFork :
                    mvprintw(4 + i, 5, "Watek %d: Czeka na lewy widelec, widelce: ", i); //2
                    break;

                case PhilosopherAction::TakeRightFork :
                    mvprintw(4 + i, 5, "Watek %d: Czeka na prawy widelec %d\%, widelce: ", i,
                             DiningPhilosophersProblem::statusOfThreads[i].ActionProgress); //2
                    break;

                case PhilosopherAction::Eat :
                    mvprintw(4 + i, 5, "Watek %d: Je %d\%, widelce: ", i,
                             DiningPhilosophersProblem::statusOfThreads[i].ActionProgress); //2
                    break;

            }

            DiningPhilosophersProblem::forksMutex.lock();

            //sprawdzam czy mam lewy
            if (DiningPhilosophersProblem::forks[i].NumberOfThreadThatHoldThisFork == i)
                printw("F%d ", i);

            if (i == threadsAmount - 1) //ostatni watek
            {
                if (DiningPhilosophersProblem::forks[0].NumberOfThreadThatHoldThisFork == i)
                    printw("F%d ", 0);

            } else // od 0 do przedostatniego watku
            {
                if (DiningPhilosophersProblem::forks[i + 1].NumberOfThreadThatHoldThisFork == i)
                    printw("F%d ", i + 1);

            }

            DiningPhilosophersProblem::forksMutex.unlock();

        }


        refresh();
        std::this_thread::sleep_for(std::chrono::milliseconds(17));

    }
    DiningPhilosophersProblem::endKeyPressed = true;

    for(int i = 0; i < threadsAmount; i++) {
        threads[i].join();
    }


    endwin();

    return 0;
}