#include <thread>
#include <mutex>
#include <vector>
#include <random>
#include "Time.hpp"

#ifndef ETAP_2_DININGPHILOSOPHERSPROBLEM_HPP
#define ETAP_2_DININGPHILOSOPHERSPROBLEM_HPP


enum class PhilosopherAction {

    Think,
    TakeLeftFork,
    TakeRightFork,
    Eat

};

struct ThreadCycleStatus {

    PhilosopherAction Action;
    uint ActionProgress;

};

struct Fork {

    bool isTaken;
    int NumberOfThreadThatHoldThisFork; //-1 - widelec odlozony

    Fork()
    {
        NumberOfThreadThatHoldThisFork = -1;
        isTaken = false;
    }
};

class DiningPhilosophersProblem {

public:
    static bool endKeyPressed;
    static std::default_random_engine generator;
    static std::vector<Fork> forks;
    static std::vector<ThreadCycleStatus> statusOfThreads;
    static std::mutex forksMutex;

    static void philosopherCycle(int philosopherNumber);

};

#endif //ETAP_2_DININGPHILOSOPHERSPROBLEM_HPP
