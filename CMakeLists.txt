cmake_minimum_required(VERSION 3.12)
project(Etap_2)


set(CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -Wall -Wextra -Wconversion -pedantic")
#set(CMAKE_CXX_FLAGS "-lncurses")

add_executable(Etap_2 main.cpp Time.cpp DiningPhilosophersProblem.cpp DiningPhilosophersProblem.hpp)

target_link_libraries(Etap_2 pthread ncurses)